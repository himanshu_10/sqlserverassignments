-- Worker Table
create database Assignments

use Assignments

create table worker(
worker_id int constraint pk primary key ,
first_name varchar(25) constraint notNull not null,
last_name varchar(25) constraint notNull not null,
salary int not null check(salary between 10000 and 25000),
joining_date datetime ,
department varchar(25) check(department in ('HR','Accts','IT','Sales'))
)

alter table worker 

insert into  worker values
  (1,'Himanshu','Rawat',25000,'2023-04-03','IT'),
   (2,'Niharika','Verma',15000,'2022-04-03','Sales'),
    (3,'Leo','Messi',25000,'2023-01-03','Accts'),
	 (4,'Vipul','Diwan',10000,'2022-12-13','HR'),
	  (5,'Praveen','Singh',20000,'2021-07-01','IT'),
	   (6,'Naveen','Jindhal',18000,'2022-01-18','Accts'),
	    (7,'Kylian','Mbappe',12000,'2023-04-03','HR'),
		 (8,'Namita','Lastname',22000,'2023-04-03','Sales'),
		  (9,'King','Kochar',14000,'2023-07-23','Accts')


select * from worker

-- Q-1. Write an SQL query to fetch �FIRST_NAME� from Worker table using the alias name as <WORKER_NAME>.
select first_name as Worker_Name from worker

-- Q-2. Write an SQL query to fetch �FIRST_NAME� from Worker table in upper case.
select upper(first_name) from worker

-- Q-3. Write an SQL query to fetch unique values of DEPARTMENT from Worker table.
select distinct department from worker 

-- Q-4. Write an SQL query to print the first three characters of  FIRST_NAME from Worker table.
select substring(first_name,1,3) 
     from worker;

-- Q-5. Write an SQL query to find the position of the alphabet (�a�) in the first name column �Amitabh� from Worker table.
select CHARINDEX('A',first_name) from Worker where first_name = 'himanshu';

-- Q-6. Write an SQL query to print the FIRST_NAME from Worker table after removing white spaces from the right side.
select rtrim(first_name) from worker;

-- Q-7. Write an SQL query to print the DEPARTMENT from Worker table after removing white spaces from the left side.
select LTRIM(department) from Worker;
 
-- Q-8. Write an SQL query that fetches the unique values of DEPARTMENT from Worker table and prints its length.
select distinct (department), len(department) as deptLenght from Worker;

-- Q-9. Write an SQL query to print the FIRST_NAME from Worker table after replacing �a� with �A�.
select replace(first_name,'a','A') from worker

-- Q-10. Write an SQL query to print the FIRST_NAME and LAST_NAME from Worker table into a single column COMPLETE_NAME. A space char should separate them.
select concat(first_name, ' ' ,last_name) as complete_name from worker ;

-- Q-11. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending.
select * from worker order by first_name asc ;

-- Q-12. Write an SQL query to print all Worker details from the Worker table order by FIRST_NAME Ascending and DEPARTMENT Descending.
select * from worker order by last_name desc ;

-- Q-13. Write an SQL query to print details for Workers with the first name as �Vipul� and �Satish� from Worker table.
select * from worker
where first_name in('Leo','Kylian')

--Q-14. Write an SQL query to print details of workers excluding first names, �Vipul� and �Satish� from Worker table.
select *
from Worker
where first_name not in('Vipul' , 'Satish')

--Q-15. Write an SQL query to print details of Workers with DEPARTMENT name as �Admin�.
select *
from Worker
where department� in('Admin')

�
--Q-16. Write an SQL query to print details of the Workers whose FIRST_NAME contains �a�.
select *
from Worker
where first_name like '%a%'

�
--Q-17. Write an SQL query to print details of the Workers whose FIRST_NAME ends with �a�.
select *
from Worker
where first_name like '%a'

--Q-18. Write an SQL query to print details of the Workers whose FIRST_NAME ends with �h� and contains six alphabets.
select *
from Worker
where first_name like '%h' and len(first_name)=6

--Q-19. Write an SQL query to print details of the Workers whose SALARY lies between 100000 and 500000.
select *
from Worker
where salary between 10000 and 500000

--Q-20. Write an SQL query to print details of the Workers who have joined in Feb�2014.
select *
from Worker
where JOINING_DATE='2023-04-03 00:00:00'

�


--Q-21. Write an SQL query to fetch the count of employees working in the department �Admin�.
select count(department) as count_admin
from Worker
where department='Admin'
group by department